"use strict"

Assert = require '../lib'
contract = Assert.contract
typed = Assert.typify
assert = require '../lib/assert'

itOriginal = window.it.bind @
it = (name, fn)->
  itOriginal name, ->
    s = async fn
    s.catch (e)->
      console.error e
      #debugger
      async fn
    return s

describe "Typed objects", ->
  s = C = obj = undefined
  beforeEach ->
    C = ->
    obj =
      simple:
        n: 1
        s: 's'
        b: false
      one:
        a: 10
      two:
        b: 'Str'
      cl: new C()
      event:
        sub: 12
      arr: []
      func: -> return @
      promise: new Promise(->)
    obj.func.inner = 10
    s = typed obj

  describe "GetSetters", ->
    it "Primitives", ->
      expect(-> s.simple.n = 20).to.not.throw()
      expect(-> s.simple.n = 's').to.throw(/Number/)
      s.simple.n.should.equal 20
      obj.simple.n.should.equal 20
      expect(-> s.simple.s = 's1').to.not.throw()
      expect(-> s.simple.s = 20).to.throw(/String/)
      s.simple.s.should.equal 's1'
      obj.simple.s.should.equal 's1'
      expect(-> s.simple.b = true).to.not.throw()
      expect(-> s.simple.b = 20).to.throw(/Boolean/)
      s.simple.b.should.equal true
      obj.simple.b.should.equal true

    it "Structures", ->
      expect(-> s.one = {a: 20}).to.not.throw()
      expect(-> s.one = 10).to.throw(/should be plain object/)
      expect(-> s.one = {a: 20, b: 2}).to.throw(/Object property b should not exist/)
      expect(-> s.one = {a: 's'}).to.throw(/Number/)
      s.one.a.should.equal 20
      obj.one.a.should.equal 20
      expect(s.one.b).to.be.undefined
      expect(obj.one.b).to.be.undefined

    it "Arrays - set", ->
      expect(-> s.arr = []).to.not.throw()
      expect(-> s.arr.push '13').to.not.throw()
      expect(-> s.arr.push 13).to.throw(/Value expected to be String/)
      expect(-> s.arr = [true]).to.throw(/Value expected to be String/)

    it "Arrays - methods", ->
      s.arr.push '13'
      s.arr.pop()
      expect(s.arr[0]).to.be.undefined
      expect(obj.arr[0]).to.be.undefined

    it "Arrays - methods 2", ->
      s.arr.push {sub: '13'}
      s.arr.map (elem)->
        elem.sub.should.equal '13'
        elem.sub = '14'
        obj.arr.map (elem)->
          elem.sub.should.equal '14'

    it "Functions", ->
      res = s.func.call {ctx: 10}
      res.ctx.should.equal 10
      s.func.inner.should.equal 10
      expect(-> s.func = 'errr').to.throw(/expected to be Function/)
      expect(-> s.func.inner = 20).to.not.throw()
      expect(-> s.func.inner = {in: 20}).to.throw(/Value expected to be Number, instead got/)
      expect(-> s.func = ->).to.not.throw()
      expect(-> s.func.inner = 20).to.throw(/not extensible/)

    it "Classes", ->
      c2 = new C()
      Z = ->
      expect(-> s.cl = new Z()).to.throw(/expected to be Function/)
      expect(-> s.cl = new C()).to.not.throw()

    it "Promises", ->
      p = new Promise(->)
      expect(-> s.promise = ->).to.throw(/expected to be Function/)
      expect(-> s.promise = p).to.not.throw()

  describe "Set method", ->
    ###
    it "Ordinal", ->
      proxied.s.set 'New'
      proxied.s.should.equal 'New'
      original.s.should.equal 'New'
      proxied.toString().should.equal original.toString()

    it "Inner", ->
      proxied.sub.t.set 'New'
      proxied.sub.t.should.equal 'New'
      original.sub.t.should.equal 'New'

    it "Object", ->
      proxied.sub.set 'New'
      proxied.sub.should.equal 'New'
      original.sub.should.equal 'New'

    it "Object - deep", ->
      proxied.sub.t.set {t2: 'New'}
      proxied.sub.t.t2.should.equal 'New'
      original.sub.t.t2.should.equal 'New'


    it "Array - set whole", ->
      proxied.arr.set ['1', '2']
      proxied.arr[0].should.equal '1'
      proxied.arr[1].should.equal '2'
      original.arr[0].should.equal '1'
      original.arr[1].should.equal '2'

    it "Array - set existing elem", ->
      proxied.arr[0].set '1'
      proxied.arr[0].should.equal '1'
      proxied.arr[1].should.equal '48'
      original.arr[0].should.equal '1'
      original.arr[1].should.equal '48'
    ###
    it "Ordinals", ->
      expect(-> s.simple.n.set 's').to.throw(/Value expected to be Number, instead got/)
      expect(-> s.simple.n.set('s', true)).to.not.throw()
      s.simple.n.should.equal 's'
      obj.simple.n.should.equal 's'

    it "Objects", ->
      expect(-> s.simple.set {o: 's'}).to.throw(/Value is required/)
      expect(-> s.simple.set({o: 's'}, true)).to.not.throw()
      s.simple.o.should.equal 's'
      obj.simple.o.should.equal 's'

    it "Arrays", ->
      expect(-> s.arr.push('asd', true)).to.not.throw()
      expect(-> s.arr[0].set(13, true)).to.not.throw()
      s.arr[0].should.equal 13


  describe "Listemn", ->
    it "Ordinals", ->
      triggered = 0
      s.event.sub.on 'set', (type, val)->
        val.newVal.should.equal
        triggered += 1
      s.event.sub = 12
      triggered.should.equal 1
      s.event.sub = 12
      triggered.should.equal 2
      s.event = {sub: 14}
      s.event.sub = 12
      triggered.should.equal 2

  getAttr = (obj)->
    if not assert.isPrimitive obj
      for k,v of obj
        x = v
        getAttr obj[k]

  afterEach ->
    getAttr(s)
    JSON.stringify(s).should.equal JSON.stringify(obj)