"use strict"
Assert = require '../lib'
contract = Assert.contract
def = Assert.def
typed = Assert.typify

describe "Contracts", ->
  describe "Arguments", ->
    it "Not exist", ->
      testFn = def Number, '*', (one)->
      expect(testFn.bind @, 1).to.not.throw()
      expect(testFn.bind @).to.throw(/Value is required/)
    it "Any - Unefined", ->
      testFn = def undefined, undefined, (one)->
      expect(testFn.bind @, 1).to.not.throw()
    it "Any - Asterisk", ->
      testFn = def '*', '*', (one)->
      expect(testFn.bind @, 1).to.not.throw()
    it "Optional", ->
      testFn = def {type: Number, required: false}, '*', (one)->
      expect(testFn.bind @).to.not.throw()
      expect(testFn.bind @, 's').to.throw(/Number/)
    it "Path - Argument№", ->
      testFn = def Number, Number, '*', (one)->
      expect(testFn.bind @, 1).to.throw(/Argument №2/)
    it "Path - one", ->
      testFn = def Number, '*', (attrOne)->
      expect(testFn.bind @, 's').to.throw(/Argument №1/)
    it "Path - two", ->
      testFn = def Number, String, '*', (attrOne, attrTwo)->
      expect(testFn.bind @, 1, 1).to.throw(/Argument №2/)

  describe "Simple Types", ->
    it "Undefined", ->
      testFn = def '*', '*', (one)->
      expect(testFn.bind @, undefined).to.not.throw()
    it "Number", ->
      testFn = def Number, '*', (one)->
      expect(testFn.bind @, 1).to.not.throw()
      expect(testFn.bind @, '1').to.throw(/Number/)
    it "Boolean", ->
      testFn = def Boolean, '*', (one)->
      expect(testFn.bind @, true).to.not.throw()
      expect(testFn.bind @, '1').to.throw(/Boolean/)
    it "String", ->
      testFn = def String, '*', (one)->
      expect(testFn.bind @, '1').to.not.throw()
      expect(testFn.bind @, 1).to.throw(/String/)
    it "Function", ->
      testFn = def Function, '*', (one)->
      expect(testFn.bind @, (->) ).to.not.throw()
      expect(testFn.bind @, '1').to.throw(/Function/)
    it "Array - direct", ->
      testFn = def Array, '*', (one)->
      expect(testFn.bind @, [] ).to.not.throw()
      expect(testFn.bind @, '1').to.throw(/Array/)

  describe "Complex Types", ->
    it "Object", ->
      testFn = def {sub: Number}, '*', (one)->
      expect(testFn.bind @, {sub: 1}).to.not.throw()
      expect(testFn.bind @, '1').to.throw(/plain object/)
      expect(testFn.bind @, {sub: '1'}).to.throw(/Number/)
      expect(testFn.bind @, {sub: '1'}).to.throw(/sub/)
      expect(testFn.bind @, {sub: 1, sub1: 2}).to.throw(/sub1/)

    it "Object - optional - typed", ->
      testFn = def {sub: Number, '*': Number}, '*', (one)->
      expect(testFn.bind @, {sub: 1, sub1: 2, sub2: 2}).to.not.throw()
      expect(testFn.bind @, {sub: 1, sub1: 2, sub2: 's'}).to.throw(/Number/)

    it "Object - optional - any", ->
      testFn = def {sub: Number, '*': '*'}, '*', (one)->
      expect(testFn.bind @, {sub: 1, sub1: 2, sub2: 's'}).to.not.throw()

    it "Object - Nested", ->
      testFn = def {sub: {subsub: Number}}, '*', (one)->
      expect(testFn.bind @, {sub: {subsub: 1}}).to.not.throw()
      expect(testFn.bind @, '1').to.throw(/plain object/)
      expect(testFn.bind @, {sub: '1'}).to.throw(/plain object/)
      expect(testFn.bind @, {sub: '1'}).to.throw(/sub/)
      expect(testFn.bind @, {sub: {}}).to.throw(/required/)
      expect(testFn.bind @, {sub: {}}).to.throw(/sub.subsub/)
      expect(testFn.bind @, {sub: {subsub: {}}}).to.throw(/Number/)
      expect(testFn.bind @, {sub: {subsub: {}}}).to.throw(/sub.subsub/)

    it "Array - [] - any", ->
      testFn = def [], '*', (one)->
      expect(testFn.bind @, [] ).to.not.throw()
      expect(testFn.bind @, [1, '2'] ).to.not.throw()
      expect(testFn.bind @, '1').to.throw(/Array/)
    it "Array - [] - typed", ->
      testFn = def [String], '*', (one)->
      expect(testFn.bind @, [] ).to.not.throw()
      expect(testFn.bind @, ['1', '2'] ).to.not.throw()
      expect(testFn.bind @, [1]).to.throw(/Path '0'/)
      expect(testFn.bind @, ['1', 2]).to.throw(/Path '1'/)

    it "Returned only", ->
      testFn = def String, (v)->
        return v
      expect(testFn.bind @, 1).to.throw(/String/)
      expect(testFn.bind @, 's').to.not.throw()

    it "In + Returned", ->
      testFn = def Number, String, (one)->
        return 's'
      expect(testFn.bind @, 1).to.not.throw()
      expect(testFn.bind @).to.throw(/Value is required/)

    it "In + returned error", ->
      testFn = def Number, String, (one)->
        return 1
      expect(testFn.bind @, 1).to.throw(/Return/)

    it "Sync - untyped", ->
      testFn = def (one)->
        return 200
      s = testFn()
      s.should.equal 200

    it "Async - untyped", ->
      testFn = def (one)-> async ->
        yield new Promise (resolve)->
          setTimeout (-> resolve(200)), 1
      s = yield testFn()
      s.should.equal 200

    it "Async - typed", ->
      testFn = def Number, String, (one)-> async ->
        yield new Promise (resolve)->
          setTimeout (-> resolve(200)), 1
      yield throws(testFn(1), /Value/)