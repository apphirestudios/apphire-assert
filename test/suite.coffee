"use strict"

Apphire = require 'apphire'

if window?
  window.app = new Apphire()
  restart = ->
    prepareRunner()
    require('./base')()

    runTests()
  restart()
  if module.hot then module.hot.accept restart

else
  eval 'global.Test = require("apphire-test")'
  global.app = new Test()
  app.serve entry: './test/suite'

