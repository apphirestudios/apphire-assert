assert = require './assert'
getNodeNames = (fn)->
  return fn.toString()
    .replace(/((\/\/.*$)|(\/\*[\s\S]*?\*\/)|(\s))/mg,'')
    .match(/^function\s*[^\(]*\(\s*([^\)]*)\)/m)[1]
    .split(/,/)
    
def = (typeDefinitions..., fn)->
  if typeDefinitions.length is 0
    return fn

  typeDefinitions = typeDefinitions.map assert.normalizeDef
  ctx = @
  outputDef = typeDefinitions.pop()
  return (args...)->
    #We can get argument names by calling function below
    #nodeNames = getNodeNames arguments.callee.caller
    #nodeNames[index]
    #But this is really ugly and unreliable hack
    for def, index in typeDefinitions
      assert.testNode args[index], def, "Argument №#{index + 1}"
    returned = fn.apply ctx, args
    if returned? and (typeof returned.then is 'function')
      return new Promise (resolve, reject)->
        returned.then ((resolved)->
          try
            assert.testNode resolved, outputDef, 'Return'
          catch e
            reject e
          resolve resolved
        ), reject
    else
      assert.testNode returned, outputDef, 'Return'
      return returned

module.exports =
  def: def.bind @