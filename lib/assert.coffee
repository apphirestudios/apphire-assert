"use strict"

typeNames = {}
typeNames[String] = 'String'
typeNames[Number] = 'Number'
typeNames[Boolean] = 'Boolean'
typeNames[Array] = 'Array'
typeNames[Object] = 'Object'
typeNames[Function] = 'Function'


baseErrorThrower = (rootName, path, message)->
  pathString = if path.length > 0 then "Path '#{path.join('.')}'. " else ""
  throw new Error "Type error in attribute #{rootName}. #{pathString}#{message}"

basePathAppender = (path, value)->
  cloned = path.slice(0)
  cloned.push value
  return cloned

normalizeDef = (def)->
  if def?.type then return def
  result = {required: true}
  if isPlainObject def
    result.type = {}
    for k,v of def
      result.type[k] = normalizeDef v
  else if (def instanceof Array)
    result.type = []
    for s in def
      result.type.push normalizeDef s
  else result.type = def
  return result

detectType = (obj)->
  if isPlainObject obj
    subSchema = {type: {}, required: true}
    for k, v of obj
      subSchema.type[k] = detectType v
    return subSchema
  else if isArray obj
    subSchema = {type: [], required: true}
    subSchema.type.push detectType obj[0]
    return subSchema
  else if isFunction obj
    subSchema = {type: Function, required: true}
    for k, v of obj
      subSchema.type[k] = detectType v
    return subSchema
  else
    return {type: detectPrimitiveType(obj), required: true}

testNode = (node, def, rootName, path)->
  return if (def.type is undefined or def.type is '*') or def.type is Undefined
  path ?= []
  throwTypeError = baseErrorThrower.bind @, rootName, path
  appendToPath = basePathAppender.bind @, path
  if not node?
    if def.required then throwTypeError "Value is required." else return

  if isPlainObject def.type
    if not isPlainObject node then throwTypeError 'Value should be plain object.'
    for name, subDef of def.type when name isnt '*'
      testNode node[name], subDef, rootName, appendToPath name
    for name, value of node when not def.type[name]?
      if not def.type['*']? then throwTypeError "Object property #{name} should not exist."
      testNode value, def.type['*'], rootName, appendToPath name

  else if isArray def.type
    if not (node instanceof Array) then throwTypeError 'Value should be Array.'
    if def.type.length > 0
      for value, index in node
        testNode value, def.type[0], rootName, appendToPath index

  else if node.constructor isnt def.type
    typeName = typeNames[def.type] or 'Function'
    throwTypeError "Value expected to be #{typeName}, instead got #{JSON.stringify(node)}"

assert = (value, typeDefinition, name)->
  testNode value, normalizeDef(typeDefinition), name or 'Unnamed'

module.exports =
  isPlainObject: isPlainObject.bind @
  isObject: isObject.bind @
  isFunction: isFunction.bind @
  isArray: isArray.bind @
  isPrimitive: isPrimitive.bind @
  isUndefined: isUndefined.bind @
  assert: assert.bind @
  testNode: testNode.bind @
  normalizeDef: normalizeDef.bind @
  detectPrimitiveType: detectPrimitiveType.bind @
  detectType: detectType.bind @