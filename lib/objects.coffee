"use strict"
assert = require './assert'
proxy = require './proxy'

defProp = (obj, name, value)->
  Object.defineProperty obj, name,
    configurable: true
    enumerable: false
    value: value

deproxy =
  get: (value, path, obj, propName)->
    if assert.isPrimitive(value)
      PrimitiveConstructor = assert.detectPrimitiveType(value)
      value = new PrimitiveConstructor(value)
    defProp value, 'set', (val)->
      obj[propName] = val
    return value

  set: (value)->
    if assert.isPrimitive(value)
      type = value?.constructor
      if not type? or type is Date then deproxied = value else deproxied = value.valueOf()
    else
      if assert.isPlainObject(value)
        deproxied = {}
      else if assert.isArray(value)
        deproxied = []
      else if assert.isFunction(value)
        deproxied = -> value.apply @, arguments
      else if assert.isObject(value)
        deproxied = {}
        deproxied.constructor = value.constructor
      else
        throw new Error "Cannot deproxy value #{value}. Unknown type"
      for k,v of value when k isnt 'set'
        deproxied[k] = deproxy.set(v)
    return deproxied

setNested = (obj, path, value)->
  cur = obj
  p = path.split('.')
  for c, index in p
    if index < p.length - 1
      if not cur[c]? #If we don't have this attribute or it is not object type
        cur[c] = {}

      cur = cur[c]
    else
      cur[c] = value
  return obj

getNested = (obj, path)->
  cur = obj
  for c, index in path.split('.')
    cur = cur[c]
    if not cur? then return
  return cur

window.listeners = {}

emitEvent = (path, name, data)->
  currentListeners = getNested(listeners, path + '.__listeners') or []
  for l in currentListeners when l.name is name
    l.fn(name, data)

emitter =
  get: (value, path)->
    defProp value, 'on', (name, fn)->
      currentListeners = getNested(listeners, path + '.__listeners') or []
      currentListeners.push {name: name, fn: fn}
      setNested listeners, path + '.__listeners', currentListeners
    defProp value, 'emit', (name, data)->
      emitEvent path, name, data
    return value

  set: (newVal, oldVal, path, parent, propName)->
    if newVal?.on? or newVal?.emit? then throw new Error 'Attribute named "on" or "emit" is not allowed in emitted object'
    currentListeners = getNested(listeners, path + '.__listeners') or []
    setNested listeners, path, {__listeners: currentListeners}
    emitEvent path, 'set', {oldVal: oldVal, newVal: newVal}
    return newVal

getAttrSchema = (schema, path)->
  if not schema.type then throw new Error 'Root schema must contain type attribute'
  current = schema
  for p in path.split('.')
    if not isNaN Number(p) then p = '0'
    if not current[p].type? then return undefined
    current = current[p]
  return current

normalizePath = (path)->
  s = path.split('.')
  for p, index in s
    if not isNaN Number(p) then s[index] = '0'
  return s.join('.')


arraize = (obj)->
  if assert.isPlainObject obj
    if obj['0']? then res = [] else res = {}
    for k, v of obj
      res[k] = arraize(v)
  else
    res = obj
  return res



module.exports =
  typify: (obj)->
    schema = assert.detectType obj
    getSchema = (path)->
      p = path.split('.')
      path = p.map((v, index)-> if index < p.length - 1 then v + '.type' else v).join('.')
      path = 'type.' + path
      return getNested(schema, path)

    setSchema = (path, value)->
      p = path.split('.')
      path = p.map((v, index)-> if index < p.length - 1 then v + '.type' else v).join('.')
      path = 'type.' + path
      value = arraize value
      setNested(schema, path, value)
      return value

    config =
      get:  (value, path)->
        if value.set
          setOrig = value.set.bind @
        defProp value, 'set', (val, force)->
          if force
            setSchema path, assert.detectType(val)
          if setOrig then setOrig(val)
      set: (newVal, oldVal, path, propName)->
        path = normalizePath(path)
        s = getSchema(path)
        if not s? or assert.isUndefined s.type
          s = setSchema path, assert.detectType newVal
        assert.testNode newVal, s, ''
        return newVal
    return proxy.powerProxy obj,
      getTransformers: [deproxy.get, config.get, emitter.get]
      setTransformers: [config.set, deproxy.set, emitter.set]

  proxify: (obj)->
    return proxy.powerProxy obj,
      getTransformers: [deproxy.get]
      setTransformers: [deproxy.set]