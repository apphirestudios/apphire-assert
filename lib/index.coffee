"use strict"
assert = require './assert'
contracts = require './contracts'
objects = require './objects'

module.exports =
  assert: assert.assert.bind @
  def: contracts.def.bind @
  proxify: objects.proxify.bind @
  typify: objects.typify.bind @
